import { TypeOrmModuleOptions } from '@nestjs/typeorm';

// - Provide data as an object
// Other ways:
// - Use a statis JSON
// - Provide data from an async service

// Connection
// Configuration object:
export const typeORMConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: ' ',
  database: 'taskman',
  // Entities which translate to SQL tables
  entities: [__dirname + '/../**/*.entity.{ts,js}'],
  synchronize: true,
};
