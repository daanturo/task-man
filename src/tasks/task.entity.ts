import { User } from 'src/auth/user.entity';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TaskStatus } from './task-status.enum';

@Entity()
export class Task extends BaseEntity {
  // Primary key
  @PrimaryGeneratedColumn()
  id: number;

  // https://orkhan.gitbook.io/typeorm/docs/entities
  // When using an entity constructor its arguments must be optional. Since ORM
  // creates instances of entity classes when loading from the database,
  // therefore it is not aware of your constructor arguments.
  constructor(
    title?: string,
    description?: string,
    status?: TaskStatus,
    user?: User,
  ) {
    super();
    this.title = title;
    this.description = description;
    this.status = status;
    this.user = user;
  }

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  status: TaskStatus;

  @ManyToOne((type) => User, (user) => user.tasks, { eager: false })
  user: User;

  // For ease of access only, it's automatically created by the relationship with `user`
  // Case sensitive?
  @Column()
  userId: number;
}
