import {
  ArgumentMetadata,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';
import { TaskStatus } from '../task-status.enum';
// Custom pipe which validates data
export class TaskStatusValidationPipe implements PipeTransform {
  readonly allowedStatuses = Object.values(TaskStatus);

  // `transform` should be implemented
  transform(value: any, metadata?: ArgumentMetadata) {
    const upcasedValue = value.toUpperCase();

    if (!this.isStatusValid(upcasedValue)) {
      throw new BadRequestException(`"${value}" status is invalid`);
    }

    return value;
  }

  private isStatusValid(status: TaskStatus): boolean {
    return this.allowedStatuses.indexOf(status) !== -1;
  }
}
