import { User } from 'src/auth/user.entity';
import { EntityRepository, Repository } from 'typeorm';
import { CreateTaskDTO } from './dto/create-task.dto';
import { GetTaskFilterDTO } from './dto/get-tasks-filter.dto';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import * as _ from 'lodash';

// Repository pattern
@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {
  async getTasks(filterDTO: GetTaskFilterDTO, user: User): Promise<Task[]> {
    const { status, search } = filterDTO;
    // Query Builder allows us to increamentally build up the query
    const query = this.createQueryBuilder('task');

    query.where('task.userId = :userID', { userID: user.id });

    // andWhere : AND WHERE conditions instead of overriding them
    if (status) {
      query.andWhere('task.status = :status', { status: status });
    }
    // Partial string matching
    if (search) {
      query.andWhere(
        '(task.title LIKE :search OR task.description LIKE :search)',
        { search: `%${search}%` },
      );
    }
    const tasks = await query.getMany();
    return tasks;
  }

  async createTask(
    createTaskDTO: CreateTaskDTO,
    taskOwner: User,
  ): Promise<Task> {
    const { title, description } = createTaskDTO;

    // There are many ways to create an object
    // Use the constructor
    const task = new Task(title, description, TaskStatus.OPEN, taskOwner);

    // const task = new Task();
    // task.title = title; task.description = description; task.status = TaskStatus.OPEN;

    // Asynchronously saves the entity in the database, if it does not exist in
    // the database then inserts, otherwise updates.
    await task.save();

    // delete task.user;
    // return task;

    // ... The `spread` operator can't be used here: ```Type '{ id: number;
    // title: string; description: string; status: TaskStatus; }' is missing the
    // following properties from type 'Task': user, hasId, save, remove, and 3
    // more.```
    // const { user, ...taskWOUser } = task;
    // return taskWOUser;

    // return Object.assign({}, task, { user: { id: taskOwner.id } });

    return _.omit(task, 'user');
  }
}
