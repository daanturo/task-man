import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { TaskRepository } from './task.repository';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';

@Module({
  imports: [
    // Array of entities or repositories that we want to include in this module

    // Make `TypeOrmModule` included in this repo instance injectable,
    // independency injection (?) throughout this module

    // The forFeature() method is used to define which repositories are
    // registered in the current scope. With that in place, we can inject the
    // Repositories (into Services) using the @InjectRepository() decorator

    TypeOrmModule.forFeature([TaskRepository]),
    AuthModule,
  ],
  controllers: [TasksController],
  providers: [TasksService],
})
export class TasksModule {}
