import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';
import { CreateTaskDTO } from './dto/create-task.dto';
import { GetTaskFilterDTO } from './dto/get-tasks-filter.dto';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';

// Declare that decorated class can be be managed by the Nest container
@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository)
    private taskRepo: TaskRepository,
  ) {}

  async getTasks(filterDTO: GetTaskFilterDTO, user: User) {
    return this.taskRepo.getTasks(filterDTO, user);
  }

  async getTaskByID(id: number, user: User): Promise<Task> {
    // id is implicit
    // Where id = id and userId = user.id
    const found = await this.taskRepo.findOne({
      where: { id, userId: user.id },
    });

    if (!found) {
      throw new NotFoundException(`Can't find a task with ID: ${id}`);
    }
    return found;
  }

  async createTask(createTaskDTO: CreateTaskDTO, user: User) {
    return this.taskRepo.createTask(createTaskDTO, user);
  }

  async deleteTaskByID(id: number, user: User) {
    const deletedTask = await this.taskRepo.delete({ id, userId: user.id });
    if (deletedTask.affected === 0) {
      throw new NotFoundException(`Task with ID ${id} not found.`);
    } else {
      return deletedTask;
    }
  }

  async updateTaskStatus(
    id: number,
    newStatus: TaskStatus,
    user: User,
  ): Promise<Task> {
    const task = await this.getTaskByID(id, user);
    task.status = newStatus;
    await task.save();
    return task;
  }
}
