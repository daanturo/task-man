import { IsString, Matches, MaxLength, MinLength } from 'class-validator';

export class AuthCredentialsPlainPasswordDTO {
  @IsString()
  @MinLength(3)
  @MaxLength(8192)
  username: string;

  @IsString()
  @MinLength(8)
  @MaxLength(8192)
  // at least 1 upper case letter
  // at least 1 lower case letter
  // at least 1 number or special character
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'You need to be stronger.',
  })
  password: string;
}

export class AuthCredentialsDTO {
  @IsString()
  @MinLength(3)
  @MaxLength(8192)
  username: string;

  @IsString()
  hashedPassword: string;
}
