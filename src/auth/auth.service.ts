import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import {
  AuthCredentialsDTO,
  AuthCredentialsPlainPasswordDTO,
} from './dto/auth-credentials.dto';
import { JWTPayload } from './jwt-payload.interface';
import { UserRepository } from './user.repository';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(authCredentialsDTO: AuthCredentialsPlainPasswordDTO) {
    return this.userRepository.signUp(authCredentialsDTO);
  }

  async signIn(
    authCredentialsPlainPasswordDTO: AuthCredentialsPlainPasswordDTO,
  ): Promise<{ accessToken: string }> {
    const username = await this.userRepository.validateUserPassword(
      authCredentialsPlainPasswordDTO,
    );
    if (!username) {
      // Tip: don't tell the attacker that the username is correct
      throw new UnauthorizedException('Invalid credentials');
    }
    const payload: JWTPayload = { username };
    const accessToken = await this.jwtService.signAsync(payload);
    return { accessToken };
  }
}
