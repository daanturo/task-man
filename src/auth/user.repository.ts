import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { EntityRepository, Repository } from 'typeorm';
import {
  AuthCredentialsDTO,
  AuthCredentialsPlainPasswordDTO,
} from './dto/auth-credentials.dto';
import { User } from './user.entity';

import * as userEntity from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  private async hashPassword(password: string, salt: string) {
    return bcrypt.hash(password, salt);
  }
  async signUp(
    authCredentialsPlainPasswordDTO: AuthCredentialsPlainPasswordDTO,
  ) {
    const { username, password } = authCredentialsPlainPasswordDTO;

    // const existedNum = await this.count({ username }); // Use when uniqueness isn't enforced at the database level

    // NOTE: Prefix the password before hashing to prevent discovering an
    // already breached (reversed hashing) one
    const salt = await bcrypt.genSalt();

    const user = new User(
      username,
      await this.hashPassword(password, salt),
      salt,
    );
    // console.log(password, user.password);

    try {
      await user.save();
    } catch (error) {
      // detail: 'Key (username)=(...) already exists.',
      if (error.code === '23505') {
        throw new ConflictException(`Username '${username}' already exists`);
      } else {
        throw new InternalServerErrorException();
      }
    }
    // Dont return anything that contains the user's password in plain text format
  }
  async validateUserPassword(
    authCredentialsPlainPasswordDTO: AuthCredentialsPlainPasswordDTO,
  ) {
    const { username, password } = authCredentialsPlainPasswordDTO;
    const user = await this.findOne({ username: username });
    if (user && (await userEntity.validatePassword(user, password))) {
      return user.username;
    } else {
      return null;
    }
  }
}
