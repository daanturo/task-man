import {
  Body,
  Controller,
  Post,
  Req,
  Request,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import {
  AuthCredentialsDTO,
  AuthCredentialsPlainPasswordDTO,
} from './dto/auth-credentials.dto';
import { GetUser } from './get-user.decorator';
import { User } from './user.entity';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  signUp(
    @Body(ValidationPipe) authCredentialsDTO: AuthCredentialsPlainPasswordDTO,
  ) {
    return this.authService.signUp(authCredentialsDTO);
  }

  @Post('/signin')
  signIn(
    @Body(ValidationPipe)
    authCredentialsPlainPasswordDTO: AuthCredentialsPlainPasswordDTO,
  ): Promise<{ accessToken: string }> {
    return this.authService.signIn(authCredentialsPlainPasswordDTO);
  }

  @Post('/test')
  // Apply guarding per-request
  // Use a custom decorator to extract the necessary arguments
  @UseGuards(AuthGuard())
  test(@GetUser() user: User) {
    console.log(user);
  }

  //
}
