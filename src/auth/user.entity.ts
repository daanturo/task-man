import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Task } from 'src/tasks/task.entity';
@Entity()
// NOTE: remember to drop the old table before changing it here
@Unique(['username']) // Declare the username column as unique
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  // Hashed, actually
  hashedPassword: string;

  @Column()
  salt: string;

  // Eager relations are always loaded automatically when relation's owner entity is loaded using find* methods.
  // Only using QueryBuilder prevents loading eager relations.
  // Eager flag cannot be set from both sides of relation - you can eager load only one side of the relationship.
  // eager:true : Whenever we retrieve the user as an object we can access user.tasks immdediately
  @OneToMany((type) => Task, (task) => task.user, { eager: true })
  tasks: Task[];

  constructor(username?: string, password?: string, salt?: string) {
    super();
    this.username = username;
    this.hashedPassword = password;
    this.salt = salt;
  }
}

export async function validatePassword(user: User, password: string) {
  const hash = await bcrypt.hash(password, user.salt);
  return user.hashedPassword === hash;
}
