import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from './user.entity';

// Create custom decorator
export const GetUser = createParamDecorator(
  // Error function
  function (_data, context: ExecutionContext): User {
    const req = context.switchToHttp().getRequest();
    return req.user;
  },
);
