import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { JWTPayload } from './jwt-payload.interface';
import { User } from './user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private UserRepository: UserRepository,
  ) {
    super({
      // Extract the JWT from the header of the request as a bearer
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // Use the same secrect that we used in 'src/auth/auth.module', for now
      secretOrKey: 'sect?',
    });
  }
  async validate(payload: JWTPayload): Promise<User> {
    const { username } = payload;
    const user = await this.UserRepository.findOne({ username: username });
    if (!user) {
      throw new UnauthorizedException();
    } else {
      return user;
    }
  }
}
