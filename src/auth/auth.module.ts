import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JWTStrategy } from './jwt.strategy';

@Module({
  imports: [
    // Out the avaiable strategies, use JSON Web Token
    PassportModule.register({ defaultStrategy: 'jwt' }),
    // Provide configurations/the way we use data with tokens in the app
    JwtModule.register({
      // Secrect to sign the payload (& header?)
      secret: 'sect?',
      signOptions: { expiresIn: 3600 },
    }),
    TypeOrmModule.forFeature([UserRepository]),
  ],
  controllers: [AuthController],
  providers: [AuthService, JWTStrategy],
  exports: [
    // guest certain JWT operations
    JWTStrategy,
    // Make it possible to guard other operations with JWT
    PassportModule,
  ],
})
export class AuthModule {}
