import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeORMConfig } from './config/typeorm.config';
import { TasksModule } from './tasks/tasks.module';
import { AuthModule } from './auth/auth.module';

@Module({
  // forRoot: for root module and this conf'll apply to all sub modules
  // make `typeORMConfig` connection avaiable everywhere
  imports: [TasksModule, TypeOrmModule.forRoot(typeORMConfig), AuthModule],
})
export class AppModule {}
