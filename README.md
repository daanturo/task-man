## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Commands used in development

```bash
nest new task-man
nest g module tasks
nest g controller tasks --no-spec
nest g service tasks --no-spec

# npm i uuid # Database will handle this

npm i class-validator class-transformer

npm i @nestjs/typeorm typeorm pg

nest g module auth
nest g controller auth --no-spec
nest g service auth --no-spec

npm i bcrypt

npm i @nestjs/jwt @nestjs/passport passport passport-jwt

```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
