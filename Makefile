yarn-start-dev:
	yarn start:dev

npm-start-dev:
	npm start dev

postgres-podman:
	podman run -d --name pg -p 8080:8080 -v postgres-volume:/var/lib/postgresql/data -e POSTGRES_PASSWORD=' ' postgres

postgres-psql:
	podman exec -it pg psql -U postgres
